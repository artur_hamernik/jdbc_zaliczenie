package zaliczenie.examiner;

import javax.swing.table.AbstractTableModel;
import java.util.List;

public class ExaminerTableModel extends AbstractTableModel {
    private final List<Examiner> examinerList;

    private final String[] columnNames = new String[] {
            "ID_egzaminatora", "ID_osoby", "Imię", "Nazwisko", "Pesel", "Email", "Haslo"
    };

    private final Class[] columnClass = new Class[]{
            Integer.class, Integer.class, String.class, String.class, String.class, String.class, String.class
    };

    public ExaminerTableModel(List<Examiner> examinerList) {
        this.examinerList = examinerList;
    }

    @Override
    public String getColumnName(int column)
    {
        return columnNames[column];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex)
    {
        return columnClass[columnIndex];
    }

    @Override
    public int getRowCount() {
        return examinerList.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Examiner row = examinerList.get(rowIndex);
        if(0 == columnIndex) {
            return row.getId_examiner();
        }
        else if(1 == columnIndex) {
            return row.getId_person();
        }
        else if(2 == columnIndex) {
            return row.getName();
        }
        else if(3 == columnIndex) {
            return row.getSurname();
        }
        else if(4 == columnIndex) {
            return row.getPesel();
        }
        else if(5 == columnIndex) {
            return row.getEmail();
        }
        else if(6 == columnIndex) {
            return row.getPassword();
        }
        return null;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
        return true;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        Examiner row = examinerList.get(rowIndex);
        if(0 == columnIndex) {
            row.setId_examiner((Integer) aValue);
        }
        else if(1 == columnIndex) {
            row.setId_person((Integer) aValue);
        }
        else if(2 == columnIndex) {
            row.setName((String) aValue);
        }
        else if(3 == columnIndex) {
            row.setSurname((String) aValue);
        }
        else if(4 == columnIndex) {
            row.setPesel((String) aValue);
        }
        else if(5 == columnIndex) {
            row.setEmail((String) aValue);
        }
        else if(6 == columnIndex) {
            row.setPassword((String) aValue);
        }
    }
}
