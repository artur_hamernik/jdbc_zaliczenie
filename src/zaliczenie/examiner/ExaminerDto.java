package zaliczenie.examiner;

public class ExaminerDto {
    private String name;
    private String surname;
    private String pesel;
    private String email;
    private String password;

    public ExaminerDto(String name, String surname, String pesel, String email, String password) {
        this.name = name;
        this.surname = surname;
        this.pesel = pesel;
        this.email = email;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
