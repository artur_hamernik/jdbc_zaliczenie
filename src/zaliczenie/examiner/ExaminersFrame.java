package zaliczenie.examiner;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class ExaminersFrame extends JFrame{
    public ExaminersFrame(String description, Component relativePosition, List<Examiner> examinerList) {
        ExaminerTableModel model = new ExaminerTableModel(examinerList);
        JTable table = new JTable(model);
        this.add(new JScrollPane(table));
        if (relativePosition == null) {
            this.setLocation(0, 0);
        } else {
            this.setLocationRelativeTo(relativePosition);
        }
        //SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
        this.setTitle(description);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
    }
}
