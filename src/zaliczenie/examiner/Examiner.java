package zaliczenie.examiner;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import static zaliczenie.Main.USER;
import static zaliczenie.Main.DB_URL;
import static zaliczenie.Main.PASS;
import static zaliczenie.Main.JDBC_DRIVER;


public class Examiner {

    private int id_person;
    private int id_examiner;
    private String name;
    private String surname;
    private String pesel;
    private String email;
    private String password;

    public Examiner(int id_examiner, int id_person,  String name, String surname, String pesel, String email, String password) {
        this.id_person = id_person;
        this.id_examiner = id_examiner;
        this.name = name;
        this.surname = surname;
        this.pesel = pesel;
        this.email = email;
        this.password = password;
    }

    public int getId_person() {
        return id_person;
    }

    public void setId_person(int id_person) {
        this.id_person = id_person;
    }

    public int getId_examiner() {
        return id_examiner;
    }

    public void setId_examiner(int id_examiner) {
        this.id_examiner = id_examiner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public static List<Examiner> getExaminers() {

        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;
        List<Examiner> examinerList = new ArrayList<>();

        try {
            Class.forName(JDBC_DRIVER);

            connection = DriverManager.getConnection(DB_URL, USER, PASS);
            connection.setAutoCommit(false);
            connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

            // Information about supported modes
            boolean isCloseAtCommitSupported = connection.getMetaData().supportsResultSetHoldability(ResultSet.CLOSE_CURSORS_AT_COMMIT);
            System.out.println(isCloseAtCommitSupported ? "Information. Database does support CLOSE_CURSORS_AT_COMMIT" : "Information. Database does NOT support CLOSE_CURSORS_AT_COMMIT");
            boolean isSensitiveSupported = connection.getMetaData().supportsResultSetType(ResultSet.TYPE_SCROLL_SENSITIVE);
            System.out.println(isSensitiveSupported ? "Information. Database does support TYPE_SCROLL_SENSITIVE" : "Information. Database does NOT support TYPE_SCROLL_SENSITIVE");

            String sql = "SELECT id_egzaminatora, o.id_osoba, imie, nazwisko, pesel, email, haslo FROM jazdy.osoba AS o " +
                    "INNER JOIN jazdy.egzaminator AS egz ON egz.id_osoba = o.id_osoba";
            stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY, ResultSet.CLOSE_CURSORS_AT_COMMIT);
            stmt.setFetchDirection(ResultSet.FETCH_FORWARD);
            stmt.setFetchSize(100);

            long startTime = System.currentTimeMillis();
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                // Uśpienie wykorzystywane do badania własności TYPE_SCROLL_SENSITIVE
                TimeUnit.MILLISECONDS.sleep(250);
                Examiner row = new Examiner(rs.getInt("id_egzaminatora"), rs.getInt("id_osoba"), rs.getString("imie"),
                        rs.getString("nazwisko"), rs.getString("pesel"), rs.getString("email"),
                        rs.getString("haslo"));
                examinerList.add(row);
                //System.out.println("Imię: " + row.name);
            }
            long endTime = System.currentTimeMillis();
            connection.commit();
            System.out.println("Execution time " + (endTime - startTime) + " ms");

        } catch (SQLException e) {
            System.err.println("Error SQL. Exception: " + e);
        } catch (Exception e) {
            System.err.println("Error. Exception: " + e);
        } finally {
            try {
                if (rs != null && !rs.isClosed()) {
                    rs.close();
                }
                if (stmt != null && !stmt.isClosed()) {
                    stmt.close();
                }
                if (connection != null && !connection.isClosed()) {
                    connection.setAutoCommit(true);
                    connection.close();
                }
            } catch (Exception e) {
                System.out.println("Error. Closing rs & stmt & connection. Exception: " + e);
            }
        }
        return examinerList;
    }

    public static int getFreeId(String who){
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;
        Integer freeId = null;

        try {
            Class.forName(JDBC_DRIVER);

            connection = DriverManager.getConnection(DB_URL, USER, PASS);
            connection.setAutoCommit(false);
            connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            String sql = null;
            if(who.equals("person")){
                sql = "SELECT MAX(id_osoba)+1 AS \"free_id\" FROM jazdy.osoba LIMIT 1";
            }
            else if(who.equals("examiner")){
                sql = "SELECT MAX(id_egzaminatora)+1 AS \"free_id\" FROM jazdy.egzaminator LIMIT 1";
            }
            stmt = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY, ResultSet.CLOSE_CURSORS_AT_COMMIT);
            stmt.setFetchDirection(ResultSet.FETCH_FORWARD);
            stmt.setFetchSize(1);

            long startTime = System.currentTimeMillis();
            rs = stmt.executeQuery(sql);

            // Information about supported modes
            boolean isCloseAtCommitSupported = connection.getMetaData().supportsResultSetHoldability(ResultSet.CLOSE_CURSORS_AT_COMMIT);
            System.out.println(isCloseAtCommitSupported ? "Information. Database does support CLOSE_CURSORS_AT_COMMIT" : "Information. Database does NOT support CLOSE_CURSORS_AT_COMMIT");
            boolean isSensitiveSupported = connection.getMetaData().supportsResultSetType(ResultSet.TYPE_SCROLL_SENSITIVE);
            System.out.println(isSensitiveSupported ? "Information. Database does support TYPE_SCROLL_SENSITIVE" : "Information. Database does NOT support TYPE_SCROLL_SENSITIVE");

            while (rs.next()) {
                freeId = rs.getInt("free_id");
            }

            connection.commit();
            long endTime = System.currentTimeMillis();

            System.out.println("Execution time " + (endTime - startTime) + " ms");

        } catch (SQLException e) {
            System.err.println("Error SQL. Exception: " + e);
        } catch (Exception e) {
            System.err.println("Error. Exception: " + e);
        } finally {
            try {
                if (rs != null && !rs.isClosed()) {
                    rs.close();
                }
                if (stmt != null && !stmt.isClosed()) {
                    stmt.close();
                }
                if (connection != null && !connection.isClosed()) {
                    connection.setAutoCommit(true);
                    connection.close();
                }
            } catch (Exception e) {
                System.out.println("Error. Closing rs & stmt & connection. Exception: " + e);
            }
        }
        return freeId;
    }
    public static boolean addPerson_PreparedStatement(Integer freeId, ExaminerDto newExaminer) {

        Connection connection = null;
        PreparedStatement pstmt = null;

        try {
            Class.forName(JDBC_DRIVER);

            connection = DriverManager.getConnection(DB_URL, USER, PASS);
            connection.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);

            String sql = "INSERT INTO jazdy.osoba(id_osoba, imie, nazwisko, pesel, email, haslo) " +
                    "VALUES(?,?,?,?,?,?);";
            pstmt = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.FETCH_FORWARD);
            pstmt.clearParameters();
            pstmt.setInt(1,freeId);
            pstmt.setString(2, newExaminer.getName());
            pstmt.setString(3, newExaminer.getSurname());
            pstmt.setString(4, newExaminer.getPesel());
            pstmt.setString(5, newExaminer.getEmail());
            pstmt.setString(6, newExaminer.getPassword());

            pstmt.executeUpdate();

        } catch (SQLException e) {
            System.err.println("Error SQL. Exception: " + e);
            return false;
        } catch (Exception e) {
            System.err.println("Error. Exception: " + e);
            return false;
        } finally {
            try {
                if (pstmt != null && !pstmt.isClosed()) {
                    pstmt.close();
                }
                if (connection != null && !connection.isClosed()) {
                    connection.close();
                    System.out.println("---  Person added  ---");
                    // Dodanie rekordu do DB?
                }
            } catch (Exception e) {
                System.out.println("Error. Closing rs & stmt & connection. Exception: " + e);
                return false;
            }
        }
        return true;
    }
    public static void addExaminerToCar_PreparedStatement(Integer id_examiner, Integer id_mord) {
        Connection connection = null;
        PreparedStatement pstmt = null;

        try {
            Class.forName(JDBC_DRIVER);

            connection = DriverManager.getConnection(DB_URL, USER, PASS);
            connection.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);

            String sql = "INSERT INTO jazdy.egzaminator_samochod_osrodek_egzamin(id_egzaminatora, nr_rejestracji, id_osrodka_egzaminacyjnego) VALUES(?,(SELECT nr_rejestracji FROM jazdy.samochod_osrodek WHERE id_osrodka = ? ORDER BY RANDOM() LIMIT 1),?);";
            pstmt = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.FETCH_FORWARD);
            pstmt.clearParameters();
            pstmt.setInt(1,id_examiner);
            pstmt.setInt(2,id_mord);
            pstmt.setInt(3,id_mord);

            pstmt.executeUpdate();

        } catch (SQLException e) {
            System.err.println("Error SQL. Exception: " + e);
        } catch (Exception e) {
            System.err.println("Error. Exception: " + e);
        } finally {
            try {
                if (pstmt != null && !pstmt.isClosed()) {
                    pstmt.close();
                }
                if (connection != null && !connection.isClosed()) {
                    connection.close();
                    System.out.println("---  Examiner to car added  ---");
                    // Dodanie rekordu do DB?
                }
            } catch (Exception e) {
                System.out.println("Error. Closing rs & stmt & connection. Exception: " + e);
            }
        }
    }
    public static void addExaminerToMord_PreparedStatement(Integer id_examiner, Integer id_mord) {
        Connection connection = null;
        PreparedStatement pstmt = null;

        try {
            Class.forName(JDBC_DRIVER);

            connection = DriverManager.getConnection(DB_URL, USER, PASS);
            connection.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);

            String sql = "INSERT INTO jazdy.egzaminator_osrodek(id_osrodka_egzaminacyjnego, id_egzaminatora) VALUES(?,?);";
            pstmt = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.FETCH_FORWARD);
            pstmt.clearParameters();
            pstmt.setInt(1,id_mord);
            pstmt.setInt(2,id_examiner);

            pstmt.executeUpdate();

        } catch (SQLException e) {
            System.err.println("Error SQL. Exception: " + e);
        } catch (Exception e) {
            System.err.println("Error. Exception: " + e);
        } finally {
            try {
                if (pstmt != null && !pstmt.isClosed()) {
                    pstmt.close();
                }
                if (connection != null && !connection.isClosed()) {
                    connection.close();
                    System.out.println("---  Examiner to MORD added  ---");
                    // Dodanie rekordu do DB?
                    addExaminerToCar_PreparedStatement(id_examiner,id_mord);
                }
            } catch (Exception e) {
                System.out.println("Error. Closing rs & stmt & connection. Exception: " + e);
            }
        }
    }
    public static void addExaminer_PreparedStatement(ExaminerDto newExaminer, Integer id_mord) {
        Integer personFreeId = getFreeId("person");
        Integer examinerFreeId = getFreeId("examiner");
        if(
                newExaminer.getName().isEmpty() ||
                newExaminer.getSurname().isEmpty() ||
                newExaminer.getEmail().isEmpty() ||
                newExaminer.getPassword().isEmpty() ||
                newExaminer.getPesel().length() != 11 ||
                !newExaminer.getPesel().matches("[0-9]+")
        ){
            System.out.println("Wrong input data for examiner details");
            return;
        }
        if(!addPerson_PreparedStatement(personFreeId, newExaminer)){
            return;
        }

        Connection connection = null;
        PreparedStatement pstmt = null;

        try {
            Class.forName(JDBC_DRIVER);

            connection = DriverManager.getConnection(DB_URL, USER, PASS);
            connection.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);

            String sql = "INSERT INTO jazdy.egzaminator(id_egzaminatora, id_osoba) VALUES(?,?);";
            pstmt = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.FETCH_FORWARD);
            pstmt.clearParameters();
            pstmt.setInt(1,examinerFreeId);
            pstmt.setInt(2,personFreeId);

            pstmt.executeUpdate();

        } catch (SQLException e) {
            System.err.println("Error SQL. Exception: " + e);
        } catch (Exception e) {
            System.err.println("Error. Exception: " + e);
        } finally {
            try {
                if (pstmt != null && !pstmt.isClosed()) {
                    pstmt.close();
                }
                if (connection != null && !connection.isClosed()) {
                    System.out.println("---  Examiner added  ---");
                    connection.close();
                    // Dodanie rekordu do DB?
                    addExaminerToMord_PreparedStatement(examinerFreeId,id_mord);
                }
            } catch (Exception e) {
                System.out.println("Error. Closing rs & stmt & connection. Exception: " + e);
            }
        }
    }

    public static String getOldPass(Integer id_examiner){
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;
        String oldPass = "";
        try {
            Class.forName(JDBC_DRIVER);

            connection = DriverManager.getConnection(DB_URL, USER, PASS);
            connection.setAutoCommit(false);
            connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            String sql = "SELECT haslo FROM jazdy.osoba WHERE id_osoba = (SELECT id_osoba FROM jazdy.egzaminator WHERE id_egzaminatora = "+ id_examiner + ") LIMIT 1";

            stmt = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY, ResultSet.CLOSE_CURSORS_AT_COMMIT);
            stmt.setFetchDirection(ResultSet.FETCH_FORWARD);
            stmt.setFetchSize(1);
            long startTime = System.currentTimeMillis();
            rs = stmt.executeQuery(sql);

            // Information about supported modes
            boolean isCloseAtCommitSupported = connection.getMetaData().supportsResultSetHoldability(ResultSet.CLOSE_CURSORS_AT_COMMIT);
            System.out.println(isCloseAtCommitSupported ? "Information. Database does support CLOSE_CURSORS_AT_COMMIT" : "Information. Database does NOT support CLOSE_CURSORS_AT_COMMIT");
            boolean isSensitiveSupported = connection.getMetaData().supportsResultSetType(ResultSet.TYPE_SCROLL_SENSITIVE);
            System.out.println(isSensitiveSupported ? "Information. Database does support TYPE_SCROLL_SENSITIVE" : "Information. Database does NOT support TYPE_SCROLL_SENSITIVE");

            while (rs.next()) {
                oldPass = rs.getString("haslo");
            }

            connection.commit();
            long endTime = System.currentTimeMillis();

            System.out.println("Execution time " + (endTime - startTime) + " ms");

        } catch (SQLException e) {
            System.err.println("Error SQL. Exception: " + e);
        } catch (Exception e) {
            System.err.println("Error. Exception: " + e);
        } finally {
            try {
                if (rs != null && !rs.isClosed()) {
                    rs.close();
                }
                if (stmt != null && !stmt.isClosed()) {
                    stmt.close();
                }
                if (connection != null && !connection.isClosed()) {
                    connection.setAutoCommit(true);
                    connection.close();
                }
            } catch (Exception e) {
                System.out.println("Error. Closing rs & stmt & connection. Exception: " + e);
            }
        }
        return oldPass;
    }
    public static boolean isValidEmailAddress(String email) {
        String ePattern = "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }
    public static String emailCheck(String updatedEmail, Integer id_examiner) {

        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;
        String email = "null";

        try {
            Class.forName(JDBC_DRIVER);

            connection = DriverManager.getConnection(DB_URL, USER, PASS);
            connection.setAutoCommit(false);
            connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

            // Information about supported modes
            boolean isCloseAtCommitSupported = connection.getMetaData().supportsResultSetHoldability(ResultSet.CLOSE_CURSORS_AT_COMMIT);
            System.out.println(isCloseAtCommitSupported ? "Information. Database does support CLOSE_CURSORS_AT_COMMIT" : "Information. Database does NOT support CLOSE_CURSORS_AT_COMMIT");
            boolean isSensitiveSupported = connection.getMetaData().supportsResultSetType(ResultSet.TYPE_SCROLL_SENSITIVE);
            System.out.println(isSensitiveSupported ? "Information. Database does support TYPE_SCROLL_SENSITIVE" : "Information. Database does NOT support TYPE_SCROLL_SENSITIVE");

            String sql = "SELECT email FROM jazdy.osoba WHERE email LIKE '" + updatedEmail + "' AND id_osoba != (SELECT id_osoba FROM jazdy.egzaminator WHERE id_egzaminatora = " + id_examiner + " LIMIT 1)";
            stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY, ResultSet.CLOSE_CURSORS_AT_COMMIT);
            stmt.setFetchDirection(ResultSet.FETCH_FORWARD);
            stmt.setFetchSize(100);

            long startTime = System.currentTimeMillis();
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                TimeUnit.MILLISECONDS.sleep(250);
                email = rs.getString("email");
            }
            long endTime = System.currentTimeMillis();
            connection.commit();
            System.out.println("Execution time " + (endTime - startTime) + " ms");

        } catch (SQLException e) {
            System.err.println("Error SQL. Exception: " + e);
        } catch (Exception e) {
            System.err.println("Error. Exception: " + e);
        } finally {
            try {
                if (rs != null && !rs.isClosed()) {
                    rs.close();
                }
                if (stmt != null && !stmt.isClosed()) {
                    stmt.close();
                }
                if (connection != null && !connection.isClosed()) {
                    connection.setAutoCommit(true);
                    connection.close();
                }
            } catch (Exception e) {
                System.out.println("Error. Closing rs & stmt & connection. Exception: " + e);
            }
        }
        return email;
    }
    public static void changeCredentials(Integer id_examiner, String email, String password) {

        String updatedEmail = "";
        String oldPassword = getOldPass(id_examiner);
        String updatedPassword = "";
        Connection connection = null;

        try {
            Class.forName(JDBC_DRIVER);

            connection = DriverManager.getConnection(DB_URL, USER, PASS);
            connection.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            connection.setAutoCommit(false);

            PreparedStatement pstmt;
            ResultSet rs = null;
            String sql = "UPDATE jazdy.osoba SET email = ?, haslo  = ?  WHERE id_osoba = (SELECT id_osoba FROM jazdy.egzaminator WHERE id_egzaminatora = ? LIMIT 1) RETURNING *";

            pstmt = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            pstmt.clearParameters();
            pstmt.setString(1, email);
            pstmt.setString(2, password);
            pstmt.setInt(3, id_examiner);

            try {
                rs = pstmt.executeQuery();
                rs.beforeFirst();
               if (rs.next()) {
                    updatedEmail  = rs.getString("email");
                    updatedPassword = rs.getString("haslo");

                    char[] passArray = updatedPassword.toCharArray();
                    boolean isUpper = false;
                    boolean isLower = false;
                    boolean isDigit = false;
                   for (char c : passArray) {
                       if (Character.isUpperCase(c)) {
                           isUpper = true;
                       } else if (Character.isLowerCase(c)) {
                           isLower = true;
                       } else if (Character.isDigit(c)) {
                           isDigit = true;
                       }
                   }
                    if (updatedEmail.equals(emailCheck(updatedEmail,id_examiner))) {
                        System.out.println("Constraint violation. Updated email = " + updatedEmail + " is used by another examiner.");
                        TimeUnit.SECONDS.sleep(15);
                        connection.rollback();
                    }
                    else if (!isValidEmailAddress(updatedEmail)) {
                        System.out.println("Constraint violation. Updated email = " + updatedEmail + " is not valid email address.");
                        TimeUnit.SECONDS.sleep(15);
                        connection.rollback();
                    }
                    else if(updatedPassword.equals(oldPassword)){
                        System.out.println("Constraint violation. Updated password = " + updatedPassword + " was recently used.");
                        TimeUnit.SECONDS.sleep(15);
                        connection.rollback();
                    }
                    else if(updatedPassword.length() < 8 || updatedPassword.equals("") || !isUpper || !isLower || !isDigit){
                       System.out.println("Constraint violation. Updated password must be 8 letters long and consist at least 1 upper case letter, 1 lowercase letter and 1 digit");
                       TimeUnit.SECONDS.sleep(15);
                       connection.rollback();
                    }
                    else {
                        TimeUnit.SECONDS.sleep(15);
                        connection.commit();
                        System.out.println("Information. Updated email = " + updatedEmail + ", updated password = " + updatedPassword);
                    }
                } else {
                    System.err.println("Information. No records were updated.");
                }
            } catch (Exception e) {
                TimeUnit.SECONDS.sleep(15);
                connection.rollback();
                System.err.println("Error. Update failed. Exception: " + e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    pstmt.close();
                } catch (Exception e) {
                    System.err.println("Error. Closing rs & pstmt. Exception: " + e);
                }
            }

        } catch (SQLException e) {
            System.err.println("Error SQL. Exception: " + e);
        } catch (Exception e) {
            System.err.println("Error. Exception: " + e);
        } finally {
            try {
                if (connection != null) {
                    // Bardzo ważne jest, aby po zakończeniu transakcji ustawić z powrotem tryb AUTOCOMMIT = FALSE;.
                    connection.setAutoCommit(true);
                    connection.close();
                }
            } catch (Exception e) {
                System.err.println("Error. Setting AutoCommit failed. Exception: " + e);
            }
        }
    }

    /*
    ---------------------
    Egzaminatorzy z samochodami i liczba egzaminow
    ---------------------
    SELECT DISTINCT egz.id_egzaminatora, o.id_osoba, imie, nazwisko, s.nr_rejestracji, marka, model, COUNT(en.id_egzaminu) AS "liczba_egzaminow" FROM jazdy.osoba AS o
    INNER JOIN jazdy.egzaminator AS egz ON egz.id_osoba = o.id_osoba
    INNER JOIN jazdy.egzaminator_osrodek AS eo ON eo.id_egzaminatora = egz.id_egzaminatora
    INNER JOIN jazdy.egzaminator_samochod_osrodek_egzamin AS eso ON eso.id_egzaminatora = eo.id_egzaminatora
    INNER JOIN jazdy.samochod AS s ON s.nr_rejestracji = eso.nr_rejestracji
    INNER JOIN jazdy.model AS mo ON mo.id_modelu = s.id_modelu
    INNER JOIN jazdy.marka AS ma ON ma.id_marki = mo.id_marki
    LEFT OUTER JOIN jazdy.egzamin AS en ON en.id_egzaminatora = egz.id_egzaminatora
    GROUP BY egz.id_egzaminatora, o.id_osoba, imie, nazwisko, s.nr_rejestracji, marka, model
    ORDER BY egz.id_egzaminatora
     */
}
