package zaliczenie.exams;

import javax.swing.table.AbstractTableModel;
import java.util.List;

public class ExaminerExamsTableModel extends AbstractTableModel {
    private final List<ExaminerExams> examinerList;

    private final String[] columnNames = new String[] {
            "ID_egzaminatora", "Imię", "Nazwisko", "Nr_Rejestracji", "Marka", "Model", "Liczba_egzaminow"
    };

    private final Class[] columnClass = new Class[]{
            Integer.class, String.class, String.class, String.class, String.class, String.class, Integer.class
    };

    public ExaminerExamsTableModel(List<ExaminerExams> examinerList) {
        this.examinerList = examinerList;
    }

    @Override
    public String getColumnName(int column)
    {
        return columnNames[column];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex)
    {
        return columnClass[columnIndex];
    }

    @Override
    public int getRowCount() {
        return examinerList.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        ExaminerExams row = examinerList.get(rowIndex);
        if(0 == columnIndex) {
            return row.getId_examiner();
        }
        else if(1 == columnIndex) {
            return row.getName();
        }
        else if(2 == columnIndex) {
            return row.getSurname();
        }
        else if(3 == columnIndex) {
            return row.getCar_id();
        }
        else if(4 == columnIndex) {
            return row.getBrand();
        }
        else if(5 == columnIndex) {
            return row.getModel();
        }
        else if(6 == columnIndex) {
            return row.getNumber_of_exams();
        }
        return null;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
        return true;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        ExaminerExams row = examinerList.get(rowIndex);
        if(0 == columnIndex) {
            row.setId_examiner((Integer) aValue);
        }
        else if(1 == columnIndex) {
            row.setName((String) aValue);
        }
        else if(2 == columnIndex) {
            row.setSurname((String) aValue);
        }
        else if(3 == columnIndex) {
            row.setCar_id((String) aValue);
        }
        else if(4 == columnIndex) {
            row.setBrand((String) aValue);
        }
        else if(5 == columnIndex) {
            row.setModel((String) aValue);
        }
        else if(6 == columnIndex) {
            row.setNumber_of_exams((Integer) aValue);
        }
    }
}
