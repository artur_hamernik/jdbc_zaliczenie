package zaliczenie.exams;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class ExaminerExamsFrame extends JFrame{
    public ExaminerExamsFrame(String description, Component relativePosition, List<ExaminerExams> examinerList) {
        ExaminerExamsTableModel model = new ExaminerExamsTableModel(examinerList);
        JTable table = new JTable(model);
        this.add(new JScrollPane(table));
        if (relativePosition == null) {
            this.setLocation(0, 0);
        } else {
            this.setLocationRelativeTo(relativePosition);
        }
        //SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
        this.setTitle(description);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
    }
}
