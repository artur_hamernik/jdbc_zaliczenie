package zaliczenie.exams;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static zaliczenie.Main.*;
import static zaliczenie.Main.PASS;

public class ExaminerExams {
    private Integer id_examiner;
    private String name;
    private String surname;
    private String car_id;
    private String brand;
    private String model;
    private Integer number_of_exams;

    public ExaminerExams(Integer id_examiner, String name, String surname, String car_id, String brand, String model, Integer number_of_exams) {
        this.id_examiner = id_examiner;
        this.name = name;
        this.surname = surname;
        this.car_id = car_id;
        this.brand = brand;
        this.model = model;
        this.number_of_exams = number_of_exams;
    }

    public Integer getId_examiner() {
        return id_examiner;
    }

    public void setId_examiner(Integer id_examiner) {
        this.id_examiner = id_examiner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getCar_id() {
        return car_id;
    }

    public void setCar_id(String car_id) {
        this.car_id = car_id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getNumber_of_exams() {
        return number_of_exams;
    }

    public void setNumber_of_exams(Integer number_of_exams) {
        this.number_of_exams = number_of_exams;
    }
    public static List<ExaminerExams> getExaminerExams() {

        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;
        List<ExaminerExams> examinerList = new ArrayList<>();

        try {
            Class.forName(JDBC_DRIVER);

            connection = DriverManager.getConnection(DB_URL, USER, PASS);
            connection.setAutoCommit(false);
            connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

            // Information about supported modes
            boolean isCloseAtCommitSupported = connection.getMetaData().supportsResultSetHoldability(ResultSet.CLOSE_CURSORS_AT_COMMIT);
            System.out.println(isCloseAtCommitSupported ? "Information. Database does support CLOSE_CURSORS_AT_COMMIT" : "Information. Database does NOT support CLOSE_CURSORS_AT_COMMIT");
            boolean isSensitiveSupported = connection.getMetaData().supportsResultSetType(ResultSet.TYPE_SCROLL_SENSITIVE);
            System.out.println(isSensitiveSupported ? "Information. Database does support TYPE_SCROLL_SENSITIVE" : "Information. Database does NOT support TYPE_SCROLL_SENSITIVE");

            String sql = "SELECT egz.id_egzaminatora, imie, nazwisko, s.nr_rejestracji, marka, model, COUNT(en.id_egzaminu) AS \"liczba_egzaminow\" FROM jazdy.osoba AS o\n" +
                    "    INNER JOIN jazdy.egzaminator AS egz ON egz.id_osoba = o.id_osoba\n" +
                    "    INNER JOIN jazdy.egzaminator_osrodek AS eo ON eo.id_egzaminatora = egz.id_egzaminatora\n" +
                    "    INNER JOIN jazdy.egzaminator_samochod_osrodek_egzamin AS eso ON eso.id_egzaminatora = eo.id_egzaminatora\n" +
                    "    INNER JOIN jazdy.samochod AS s ON s.nr_rejestracji = eso.nr_rejestracji\n" +
                    "    INNER JOIN jazdy.model AS mo ON mo.id_modelu = s.id_modelu\n" +
                    "    INNER JOIN jazdy.marka AS ma ON ma.id_marki = mo.id_marki\n" +
                    "    LEFT OUTER JOIN jazdy.egzamin AS en ON en.id_egzaminatora = egz.id_egzaminatora\n" +
                    "    GROUP BY egz.id_egzaminatora, o.id_osoba, imie, nazwisko, s.nr_rejestracji, marka, model\n" +
                    "    ORDER BY egz.id_egzaminatora";

            stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY, ResultSet.CLOSE_CURSORS_AT_COMMIT);
            stmt.setFetchDirection(ResultSet.FETCH_FORWARD);
            stmt.setFetchSize(100);

            long startTime = System.currentTimeMillis();
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                TimeUnit.MILLISECONDS.sleep(250);
                ExaminerExams row = new ExaminerExams(
                        rs.getInt("id_egzaminatora"),
                        rs.getString("imie"),
                        rs.getString("nazwisko"),
                        rs.getString("nr_rejestracji"),
                        rs.getString("marka"),
                        rs.getString("model"),
                        rs.getInt("liczba_egzaminow"));
                examinerList.add(row);
            }
            long endTime = System.currentTimeMillis();
            connection.commit();
            System.out.println("Execution time " + (endTime - startTime) + " ms");

        } catch (SQLException e) {
            System.err.println("Error SQL. Exception: " + e);
        } catch (Exception e) {
            System.err.println("Error. Exception: " + e);
        } finally {
            try {
                if (rs != null && !rs.isClosed()) {
                    rs.close();
                }
                if (stmt != null && !stmt.isClosed()) {
                    stmt.close();
                }
                if (connection != null && !connection.isClosed()) {
                    connection.setAutoCommit(true);
                    connection.close();
                }
            } catch (Exception e) {
                System.out.println("Error. Closing rs & stmt & connection. Exception: " + e);
            }
        }
        return examinerList;
    }
}
    /*
    ---------------------
    Egzaminatorzy z samochodami i liczba egzaminow
    ---------------------
    SELECT DISTINCT egz.id_egzaminatora, o.id_osoba, imie, nazwisko, s.nr_rejestracji, marka, model, COUNT(en.id_egzaminu) AS "liczba_egzaminow" FROM jazdy.osoba AS o
    INNER JOIN jazdy.egzaminator AS egz ON egz.id_osoba = o.id_osoba
    INNER JOIN jazdy.egzaminator_osrodek AS eo ON eo.id_egzaminatora = egz.id_egzaminatora
    INNER JOIN jazdy.egzaminator_samochod_osrodek_egzamin AS eso ON eso.id_egzaminatora = eo.id_egzaminatora
    INNER JOIN jazdy.samochod AS s ON s.nr_rejestracji = eso.nr_rejestracji
    INNER JOIN jazdy.model AS mo ON mo.id_modelu = s.id_modelu
    INNER JOIN jazdy.marka AS ma ON ma.id_marki = mo.id_marki
    LEFT OUTER JOIN jazdy.egzamin AS en ON en.id_egzaminatora = egz.id_egzaminatora
    GROUP BY egz.id_egzaminatora, o.id_osoba, imie, nazwisko, s.nr_rejestracji, marka, model
    ORDER BY egz.id_egzaminatora
     */