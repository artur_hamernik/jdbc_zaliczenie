package zaliczenie;

import zaliczenie.examiner.Examiner;
import zaliczenie.examiner.ExaminerDto;
import zaliczenie.examiner.ExaminersFrame;
import zaliczenie.exams.ExaminerExams;
import zaliczenie.exams.ExaminerExamsFrame;

import javax.swing.*;
import java.util.List;

import static zaliczenie.examiner.Examiner.changeCredentials;
import static zaliczenie.examiner.Examiner.addExaminer_PreparedStatement;
import static zaliczenie.examiner.Examiner.getExaminers;
import static zaliczenie.exams.ExaminerExams.getExaminerExams;

public class Main{

    public static final String JDBC_DRIVER = "org.postgresql.Driver";
    public static final String DB_URL = "jdbc:postgresql://195.150.230.210:5434/2020_hamernik_artur";
    public static final String USER = "2020_hamernik_artur";
    public static final String PASS = "31996";

    public static void main(String[] args) {
        int task = 3;
        switch (task) {
            case 1://Wyswietlanie szegółowych danych o egzaminatorach
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        // @TP Laboratory(description, windowRelativePosition)
                        List<Examiner> examinerList = getExaminers();
                        List<ExaminerExams> examinerExamsList = getExaminerExams();
                        ExaminersFrame tableT1 = new ExaminersFrame("Examiners", null, examinerList);
                        ExaminerExamsFrame tableT2 = new ExaminerExamsFrame("Exams of examiners" , tableT1, examinerExamsList);
                    }
                });
                break;
            case 2://Dodanie egzaminatora do bazy oraz wyswietlanie danych o nich
                ExaminerDto newExaminer = new ExaminerDto("Anna","Ciochoń","95041290876","cyaan@yahoo.com.pl","Awokado78");
                addExaminer_PreparedStatement(newExaminer ,31);
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        // @TP Laboratory(description, windowRelativePosition)
                        List<Examiner> examinerList = getExaminers();
                        List<ExaminerExams> examinerExamsList = getExaminerExams();
                        ExaminersFrame tableT1 = new ExaminersFrame("Examiners", null, examinerList);
                        ExaminerExamsFrame tableT2 = new ExaminerExamsFrame("Exams of examiners" , tableT1, examinerExamsList);
                    }
                });
                break;

            case 3://Zmiana emailu i hasla egzaminatora o podanym ID, z sprawdzeniem maila i hasla oraz ROLLBACK
                changeCredentials(37, "cyann@gmail.com", "RatlereK919");
                break;
        }
    }
}
/*ID OSRODKÓW DO KTÓRYCH MOŻNA DODAC NOWEGO EGZAMINATORA, ORAZ AUTA PRZYPISANE DO TYCH OSRODKOW, AUTO JEST PRZYDZIELANE LOSOWO
    3	"eih 2106"
    3	"dyv 1324"
    3	"dtm 4387"
    5	"ygb 7633"
    7	"ngg 6994"
    9	"hgh 7566"
    9	"fve 3877"
    11	"fve 3877"
    13	"pho 5397"
    13	"hgh 7566"
    15	"tid 1852"
    15	"soh 5191"
    15	"fzx 1344"
    15	"kes 3325"
    17	"kys 3186"
    17	"irc 5152"
    17	"yfh 0269"
    17	"hmh 2719"
    19	"gvy 5455"
    19	"mpf 4932"
    21	"gvy 5455"
    21	"sey 5178"
    21	"vmk 3626"
    21	"xbi 3855"
    23	"zfv 9424"
    23	"orh 1115"
    23	"fra 8190"
    23	"dks 1891"
    23	"ycn 3761"
    25	"stx 8693"
    25	"orh 1115"
    27	"tjh 8935"
    27	"ksy 6209"
    29	"tfh 3673"
    31	"kys 3186"
    31	"ksy 6209"
    31	"gcv 6927"
    31	"uzl 9926"
    31	"kcr 7355"
    33	"tml 9181"
    33	"sck 7543"
    33	"kes 3325"
    33	"gcv 6927"
    35	"leb 5153"
    35	"vyo 4959"
    37	"bjg 0058"
    37	"fvn 6728"
    37	"ngg 6994"
    39	"leb 5153"
    39	"stx 8693"
    41	"ksz 2101"
    41	"dyv 1324"
    41	"vck 4648"
    41	"yvl 0200"
    43	"fcj 0342"
    45	"ztd 2484"
    45	"ven 1123"
    45	"tfh 3673"
    45	"hmh 2719"
    47	"tml 9181"
    47	"tjh 8935"
    49	"vnw 6426"
    49	"pho 5397"
    49	"mwh 3933"
    51	"mwh 3933"
    53	"yfh 0269"
    55	"ztd 2484"
    55	"fcj 0342"
    57	"coi 1051"
    57	"tvd 3729"
    57	"sey 5178"
    57	"tid 1852"
    59	"vnw 6426"
    63	"ksz 2101"
    63	"mpk 2031"
    63	"ven 1123"
    63	"gsw 4950"
    63	"dks 1891"
    63	"eih 2106"
    65	"bjg 0058"
    65	"kde 0824"
    65	"fra 8190"
 */
